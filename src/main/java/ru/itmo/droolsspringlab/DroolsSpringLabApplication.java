package ru.itmo.droolsspringlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsSpringLabApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroolsSpringLabApplication.class, args);
    }

}
