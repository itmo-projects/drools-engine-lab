package ru.itmo.droolsspringlab.model;

import java.util.Date;

public record Order(Date purchaseDate, int amount, String user, int score) {
}