package ru.itmo.droolsspringlab.model;

import lombok.Data;

@Data
public class OrderPoints {
    private Integer orderPoints;
}
