package ru.itmo.droolsspringlab.resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.droolsspringlab.model.Order;
import ru.itmo.droolsspringlab.model.OrderPoints;
import ru.itmo.droolsspringlab.service.OrderService;

@RestController
@RequestMapping("/api/loyalti/order")
public record OrderResource(OrderService orderService) {

    @RequestMapping("/submit")
    public OrderPoints submitOrderForPoints(@RequestBody Order order) {
        return orderService.checkOrder(order);
    }
}
