package ru.itmo.droolsspringlab.service;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import ru.itmo.droolsspringlab.model.Order;
import ru.itmo.droolsspringlab.model.OrderPoints;

public record OrderService(KieContainer kieContainer) {
    public OrderPoints checkOrder(Order order) {
        OrderPoints orderPoints = new OrderPoints();
        KieSession kieSession = kieContainer.newKieSession();
        kieSession.setGlobal(OrderPoints.class.getSimpleName(), orderPoints);
        kieSession.insert(order);
        kieSession.fireAllRules();
        kieSession.dispose();

        return orderPoints;
    }
}