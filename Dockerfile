FROM openjdk:17-alpine

ENV TZ=Europe/Moscow

RUN apk add tzdata curl
RUN cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY /drools*.jar /drools-app.jar

EXPOSE 8081

ENTRYPOINT [ "java", 				\
             "-Duser.timezone=$TZ", \
             "-jar",           		\
             "/drools-app.jar"      \
]