# ITMO University | Университет ИТМО
### Databases and knowledge | Базы данных и знаний

## Project description
[Drools](https://www.drools.org/) - Business Rules Management System (BRMS) solution, that provides business rules engine<br>
Project presents simple REST Spring Boot Application for getting scores based on purchase data